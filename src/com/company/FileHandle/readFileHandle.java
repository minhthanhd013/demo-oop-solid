package com.company.FileHandle;

/**
 * This class is responsible for read file, but in this case it has to implement method writeFile.
 */
public class readFileHandle implements ReadAndWriteFileHandle{

    @Override
    public void readFile() {

    }

    @Override
    public void writeFile() {

    }

    @Override
    public String getData() {
        return null;
    }
}
