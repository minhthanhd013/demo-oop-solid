package com.company.FileHandle;

/**
 * case of violation of Interface segregation principle
 */
public interface ReadAndWriteFileHandle {
    public void readFile();
    public void writeFile();
    public String getData();
}
