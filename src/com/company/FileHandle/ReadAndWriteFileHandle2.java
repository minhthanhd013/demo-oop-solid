package com.company.FileHandle;

/**
 * Case: applied Interface segregation principle to separate read and write file.
 */
public interface ReadAndWriteFileHandle2 {
    public String getData();
}
