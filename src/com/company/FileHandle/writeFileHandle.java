package com.company.FileHandle;
/**
 * This class is responsible for write file, but in this case it has to implement method readFile.
 */
public class writeFileHandle implements ReadAndWriteFileHandle{
    @Override
    public void readFile() {

    }

    @Override
    public void writeFile() {

    }

    @Override
    public String getData() {
        return null;
    }
}
