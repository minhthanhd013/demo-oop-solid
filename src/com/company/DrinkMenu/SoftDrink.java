package com.company.DrinkMenu;

import java.util.ArrayList;

public class SoftDrink extends DrinkMenu{

    private ArrayList<String> softDrink;

    public SoftDrink() {
        System.out.println("SoftDrink constructor without params call!");
    }
    /**
     * Constructor using param ArrayList<String> softDrink call inside SoftDrink class.
     * @param softDrink
     */
    public SoftDrink(ArrayList<String> softDrink) {
        this.softDrink = softDrink;
    }

    @Override
    public void getMenu() {
        System.out.println("Soft drink menu call!");
        for (String temp: softDrink
        ) {
            System.out.println("\t"+temp);
        }
    }
}
