package com.company.DrinkMenu;

import java.util.ArrayList;

public class AlcoholDrink extends DrinkMenu implements AlcoholConcentration{

    private ArrayList<String>  alcoholDrink;

    public AlcoholDrink() {
        System.out.println("AlcoholDrink constructor without params call!");
    }

    public AlcoholDrink(ArrayList<String> alcoholDrink) {
        System.out.println("AlcoholDrink constructor with params call!");
        this.alcoholDrink = alcoholDrink;
    }

    @Override
    public void getMenu() {
        System.out.println("Alcohol drink menu call!");
        for (String temp: alcoholDrink
        ) {
            System.out.println("\t"+temp+",\tNong do con: " + this.checkConcentration(temp));

        }
    }

    @Override
    public float checkConcentration(String name) {
        float result = 0.0F;
        switch (name) {
            case "Vodka":
                result = 0.55F;
                return result;
            case "Rum":
                result = 0.12F;
                return result;
            case "Gin":
                result = 0.45F;
                return result;
            case "Tequila":
                result = 0.38F;
                return result;
            default:
                return result;
        }
    }
}
