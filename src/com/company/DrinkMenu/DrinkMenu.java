package com.company.DrinkMenu;

import com.company.Menu;

public abstract class DrinkMenu extends Menu {
    public DrinkMenu() {
        super.setMenuType("Drink Menu");
    }
    public abstract void getMenu();
}
