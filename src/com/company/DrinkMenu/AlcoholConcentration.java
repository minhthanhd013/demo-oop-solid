package com.company.DrinkMenu;

public interface AlcoholConcentration {

    /**
     * Apply Liskov substitution principle to get alcohol concentration from alcohol drinks.
     * @param name
     * @return
     */
    public float checkConcentration(String name);
}
