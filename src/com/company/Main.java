package com.company;

import com.company.DataHandle.*;
import com.company.DrinkMenu.AlcoholDrink;
import com.company.DrinkMenu.DrinkMenu;
import com.company.DrinkMenu.SoftDrink;
import com.company.FoodMenu.BreakfastMenu;
import com.company.FoodMenu.DinnerMenu;
import com.company.FoodMenu.FoodMenu;
import com.company.FoodMenu.LunchMenu;



public class Main {

    public static void main(String[] args) {
        // Apply Single responsibility principle and Open/Closed principle.
        DataGenerator data1 = new BreakfastData();
        DataGenerator data2 = new LunchData();
        DataGenerator data3 = new DinnerData();
        DataGenerator data4 = new SoftDrinkData();
        DataGenerator data5 = new AlcoholDrinkData();
        // Demo abstract class.
        FoodMenu breakfastMenu = new BreakfastMenu(data1.getData());
        breakfastMenu.getMenu();

        FoodMenu lunchMenu = new LunchMenu(data2.getData());
        lunchMenu.getMenu();

        FoodMenu dinnerMenu = new DinnerMenu(data3.getData());
        dinnerMenu.getMenu();

        // Demo inheritance
        // These 3 output (FoodMenu) will print out the type of menu (result: Food Menu) using parent method getMenuType() (Menu class).
        System.out.println(breakfastMenu.getMenuType());
        System.out.println(lunchMenu.getMenuType());
        System.out.println(dinnerMenu.getMenuType());

        // Apply Liskov substitution principle to get Alcohol Concentration (nong do con) in alcohol drinks.
        DrinkMenu softDrinkMenu = new SoftDrink(data4.getData());
        softDrinkMenu.getMenu();

        DrinkMenu alcoholDrinkMenu = new AlcoholDrink(data5.getData());
        alcoholDrinkMenu.getMenu();
        // Demo Interface segregation principle in FileHandle package

        // Demo Dependency inversion principle
        MenuManagement menuManagement = new MenuManagement();
        menuManagement.setMenu(new BreakfastMenu(data1.getData()));
        menuManagement.getData();

        menuManagement.setMenu(new LunchMenu(data2.getData()));
        menuManagement.getData();

        menuManagement.setMenu(new DinnerMenu(data3.getData()));
        menuManagement.getData();
    }
}
