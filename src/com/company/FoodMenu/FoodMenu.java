package com.company.FoodMenu;

import com.company.Menu;

/**
 * Demo abstract class. Food menu could contain breakfast menu, lunch menu, dinner menu
 *
 */
public abstract class FoodMenu extends Menu {
    public FoodMenu() {
        super.setMenuType("Food Menu");
    }

    public abstract void getMenu();
}
