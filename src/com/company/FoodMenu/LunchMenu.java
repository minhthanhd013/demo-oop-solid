package com.company.FoodMenu;

import java.util.ArrayList;
/**
 * This class is use to handle lunch menu,
 * extended by FoodMenu abstract class.
 */
public class LunchMenu extends FoodMenu{
    private ArrayList<String> lunchMenu;


    /**
     * Constructor using param ArrayList<String> lunchMenu call inside LunchMenu class.
     * @param lunchMenu
     */
    public LunchMenu(ArrayList<String> lunchMenu) {
        System.out.println("Lunch constructor with param call!");
        this.lunchMenu = lunchMenu;
    }

    // Getter and Setter.
    public ArrayList<String> getLunchMenu() {
        return lunchMenu;
    }

    public void setLunchMenu(ArrayList<String> lunchMenu) {
        this.lunchMenu = lunchMenu;
    }
    /**
     * getMenu() call inside of lunchMenu class to display the list of lunch menu.
     */

    @Override
    public void getMenu() {
        System.out.println("Lunch menu call!");

        for (String temp: lunchMenu
        ) {
            System.out.println("\t"+temp);
        }
    }
}
