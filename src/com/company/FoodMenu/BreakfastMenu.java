package com.company.FoodMenu;

import java.util.ArrayList;

/**
 * This class is use to handle breakfast menu,
 * extended by FoodMenu abstract class.
 */
public class BreakfastMenu extends FoodMenu{
    private ArrayList<String> breakfastMenu;

    /**
     * Constructor using param ArrayList<String> breakfastMenu call inside BreakfastMenu class.
     * @param breakfastMenu
     */
    public BreakfastMenu(ArrayList<String> breakfastMenu) {
        System.out.println("Breakfast constructor with param call!");
        this.breakfastMenu = breakfastMenu;
    }


    /**
     * getMenu() call inside of BreakfastMenu class to display the list of breakfast menu.
     */
    @Override
    public void getMenu() {
        System.out.println("Breakfast menu call!");
        for (String temp: breakfastMenu
             ) {
            System.out.println("\t"+temp);
        }

    }
}
