package com.company.FoodMenu;

import java.util.ArrayList;

/**
 * This class is use to handle dinner menu,
 * extended by FoodMenu abstract class.
 */
public class DinnerMenu extends FoodMenu{
    private ArrayList<String> dinnerMenu;

    /**
     * Constructor using param ArrayList<String> dinnerMenu call inside DinnerMenu class.
     * @param dinnerMenu
     */
    public DinnerMenu(ArrayList<String> dinnerMenu) {
        System.out.println("Dinner constructor with param call!");
        this.dinnerMenu = dinnerMenu;
    }


    public ArrayList<String> getDinnerMenu() {
        return dinnerMenu;
    }

    public void setDinnerMenu(ArrayList<String> dinnerMenu) {
        this.dinnerMenu = dinnerMenu;
    }
    /**
     * getMenu() call inside of DinnerMenu class to display the list of Dinner menu.
     */
    @Override
    public void getMenu() {
        System.out.println("Dinner menu call!");
        for (String temp: dinnerMenu
        ) {
            System.out.println("\t"+temp);
        }
    }
}
