package com.company.DataHandle;

import java.util.ArrayList;

/**
 * Apply Single responsibility priciple (SRP) and Open/Closed principle.
 * This interface has only 1 function that is create the input data for FoodMenu.
 * When ever user need to get any data, just implement the interface DataGenerator and return ArrayList
 */
public interface DataGenerator {
    public ArrayList<String> getData();
}
