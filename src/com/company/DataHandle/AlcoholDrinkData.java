package com.company.DataHandle;

import java.util.ArrayList;

public class AlcoholDrinkData implements DataGenerator{

    @Override
    public ArrayList<String> getData() {
        ArrayList<String> temp = new ArrayList<>();
        temp.add("Tequila");
        temp.add("Gin");
        temp.add("Vodka");
        temp.add("Rum");
        return temp;
    }
}
