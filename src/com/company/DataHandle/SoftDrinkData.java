package com.company.DataHandle;

import java.util.ArrayList;

public class SoftDrinkData implements DataGenerator{
    @Override
    public ArrayList<String> getData() {
        ArrayList<String> temp = new ArrayList<>();
        temp.add("Nuoc ngot 7up");
        temp.add("Nuoc suoi");
        temp.add("Nuoc ngot coca");
        return temp;
    }
}
